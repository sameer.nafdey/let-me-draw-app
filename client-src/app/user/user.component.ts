import { Component, OnInit } from '@angular/core';
import { AuthService } from '../providers/auth.service.ts.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: firebase.User;
  constructor(private authService: AuthService) {
    this.authService.authStateChanged.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {}
}
