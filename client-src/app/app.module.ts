import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// for AngularFireAuth
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';

// import {} from './angular-canvas-painter.js';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { TitlebarComponent } from './titlebar/titlebar.component';
import { FriendsListComponent } from './friends-list/friends-list.component';
import { CanvasBoardsComponent } from './canvas-boards/canvas-boards.component';
import { NavTabsComponent } from './nav-tabs/nav-tabs.component';
import { InvitefriendComponent } from './friends-list/invitefriend/invitefriend.component';
import { AuthService } from './providers/auth.service.ts.service';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    TitlebarComponent,
    FriendsListComponent,
    CanvasBoardsComponent,
    NavTabsComponent,
    InvitefriendComponent,
    LandingpageComponent,
    NavbarComponent,
    UserComponent
  ],
  imports: [
    // BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
