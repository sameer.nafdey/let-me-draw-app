export class UserModel {
  constructor(
    private email: string,
    private avatarUrl: string,
    private displayName: string,
    private tokenID: string
  ) {}

  public getEmail(): string {
    return this.email;
  }

  public getAvatarUrl(): string {
    return this.avatarUrl;
  }

  public getName(): string {
    return this.displayName;
  }
}
