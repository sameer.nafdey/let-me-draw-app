import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { UserComponent } from './user/user.component';

// import { ComponentsComponent } from './components/components.component';
// import { LandingComponent } from './examples/landing/landing.component';
// import { LoginComponent } from './examples/login/login.component';
// import { ProfileComponent } from './examples/profile/profile.component';
// import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';

const routes: Routes = [
  { path: '', component: LandingpageComponent },
  { path: 'my', component: UserComponent }
  // { path: 'nucleoicons',          component: NucleoiconsComponent },
  // { path: 'examples/landing',     component: LandingComponent },
  // { path: 'examples/login',       component: LoginComponent },
  // { path: 'examples/profile',     component: ProfileComponent }
];

@NgModule({
  imports: [CommonModule, BrowserModule, RouterModule.forRoot(routes)],
  exports: []
})
export class AppRoutingModule {}
