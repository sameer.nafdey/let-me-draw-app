import { Injectable, Output, EventEmitter } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { UserModel } from '../model/User.model';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class AuthService {
  isAuth = false;
  authColor = 'warn';
  user: firebase.User;

  authStateChanged = new EventEmitter<firebase.User>();

  constructor(public afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(
      user => this._changeState(user),
      error => console.error(error)
    );
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = this._getProvider('google');
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth.signInWithPopup(provider).then(res => {
        console.log('response: ', JSON.stringify(res));
        resolve(res);
      });
    });
  }

  doGoogleLogout() {
    this.afAuth.auth.signOut().then(res => {
      console.log('Signed out: ', JSON.stringify(res));
      this.authStateChanged.emit(null);
    });
  }

  private _changeState(user: firebase.User = null) {
    if (user) {
      this.isAuth = true;
      this.authColor = 'primary';
      this.user = user;
      this.authStateChanged.emit(user);
    } else {
      this.isAuth = false;
      this.authColor = 'warn';
      this.user = null;
    }
  }

  // private _getUserInfo(user: any): any {
  //   if (!user) {
  //     return {};
  //   }
  //   const data = user.providerData[0];
  //   return {
  //     name: data.displayName,
  //     avatar: data.photoURL,
  //     email: data.email,
  //     provider: data.providerId
  //   };
  // }

  private _getProvider(from: string) {
    switch (from) {
      case 'twitter':
        return null; //AuthProviders.Twitter;
      case 'facebook':
        return null; // AuthProviders.Facebook;
      case 'github':
        return null; //AuthProviders.Github;
      case 'google':
        return new firebase.auth.GoogleAuthProvider();
    }
  }
}
