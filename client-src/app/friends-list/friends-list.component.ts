import { Component, OnInit } from '@angular/core';
import { Friend, FriendStatus } from './friend.model';

@Component({
  selector: 'app-friends-list',
  templateUrl: './friends-list.component.html',
  styleUrls: ['./friends-list.component.css']
})
export class FriendsListComponent implements OnInit {
  friendList: Friend[] = [
    new Friend(
      'sameer@gmail.com',
      'Sameer Nafdey',
      true,
      'https://lh3.googleusercontent.com/-kMjtulsTQZw/AAAAAAAAAAI/AAAAAAAAAAA/APUIFaOJ7ql0nhs3etB0M6PhLpCNiRPq3Q/s32-c-mo/photo.jpg',
      FriendStatus.ONLINE
    ),
    new Friend(
      'monika@gmail.com',
      'Monika',
      true,
      'https://lh3.googleusercontent.com/-0rW1VA_oZl4/AAAAAAAAAAI/AAAAAAAAAAA/APUIFaPqsigJmUSkM2edfDXCv9A8axafHQ/s32-c-mo/photo.jpg',
      FriendStatus.INVITED
    )
  ];

  constructor() {}

  ngOnInit() {}
}
