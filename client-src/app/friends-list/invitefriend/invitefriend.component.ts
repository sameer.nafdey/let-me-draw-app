import { Component, OnInit } from '@angular/core';
import { Friend, FriendStatus } from '../friend.model';

@Component({
  selector: 'app-invitefriend',
  templateUrl: './invitefriend.component.html',
  styleUrls: ['./invitefriend.component.css']
})
export class InvitefriendComponent implements OnInit {
  public model = new Friend('');

  constructor() {}

  ngOnInit() {}

  sendInvite() {
    console.log(
      'Test event binding on form submit',
      JSON.stringify(this.model)
    );
  }
}
