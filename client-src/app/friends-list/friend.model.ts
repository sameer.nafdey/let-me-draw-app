export class Friend {
  public email: string;
  public displayName: string;
  public invitationAccepted: boolean;
  public avatarUrl: string;
  public status: FriendStatus;
  constructor(
    email: string,
    displayName = '',
    invitationAccepted = false,
    avatarUrl = '',
    status = FriendStatus.INVITED
  ) {
    this.email = email;
    this.displayName = displayName;
    this.invitationAccepted = invitationAccepted;
    this.avatarUrl = avatarUrl;
    this.status = status;
  }
}

export enum FriendStatus {
  ONLINE = 'online',
  OFFLINE = 'offline',
  INVITED = 'invited'
}
