import {
  Component,
  OnInit,
  Inject,
  Renderer,
  ElementRef,
  ViewChild
} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { NavbarComponent } from './shared/navbar/navbar.component';

import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';
import { AuthService } from './providers/auth.service.ts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user: firebase.User;
  title = "Let'sDraw";

  private _router: Subscription;
  @ViewChild(NavbarComponent)
  navbar: NavbarComponent;

  constructor(
    private authService: AuthService,
    private renderer: Renderer,
    private router: Router,
    private element: ElementRef,
    public location: Location,
    @Inject(DOCUMENT) private document: any
  ) {
    this.authService.authStateChanged.subscribe(user => {
      this.user = user;
      if (this.user) {
        this.router.navigate(['/my']);
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  ngOnInit() {
    const navbar: HTMLElement = this.element.nativeElement.children[0]
      .children[0];
    this._router = this.router.events.subscribe((event: NavigationEnd) => {
      if (window.outerWidth > 991) {
        window.document.children[0].scrollTop = 0;
      } else {
        window.document.activeElement.scrollTop = 0;
      }
      this.navbar.sidebarClose();

      this.renderer.listenGlobal('window', 'scroll', (event: any) => {
        const number = window.scrollY;
        let _location = this.location.path();
        _location = _location.split('/')[2];

        if (number > 150 || window.pageYOffset > 150) {
          navbar.classList.remove('navbar-transparent');
        } else if (
          _location !== 'login' &&
          this.location.path() !== '/nucleoicons'
        ) {
          // remove logic
          navbar.classList.add('navbar-transparent');
        }
      });
    });
  }
}
