import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvasBoardsComponent } from './canvas-boards.component';

describe('CanvasBoardsComponent', () => {
  let component: CanvasBoardsComponent;
  let fixture: ComponentFixture<CanvasBoardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CanvasBoardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CanvasBoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
