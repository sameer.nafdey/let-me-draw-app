if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'local';
  process.env.JWT_SIGNATURE = 'b8f6dfb0290199ed753321e8009a27cee48bbdeca81c668bd030dd880cc602ff';
  console.log(`Settting process.env.NODE_ENV : ${process.env.NODE_ENV}`)
}

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const indexRouter = require('./routes/index');
const angularRouter = require('./routes/angular');
const apiRouter = require('./routes/api');
const app = express();
const subdomain = require('express-subdomain');

const allowedOrigins = [
  // 'http://localhost:3000',
  'https://letmedraw.herokuapp.com/',
  'https://whispering-sierra-67512.herokuapp.com/'
];
app.use(cors({
  origin: function (origin, callback) {
    //console.log("What is the origin of request? ", origin)
    // allow requests with no origin 
    // (like mobile apps or curl requests)
    if (process.env.NODE && ~process.env.NODE.indexOf("staging")) {
      if (allowedOrigins.indexOf(origin) === -1) {
        var msg = 'The CORS policy for this site does not ' +
          'allow access from the specified Origin.';
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    } else {
      if (!origin) return callback(null, true);
    }


  }
}));

var {
  mongoose
} = require('./db/mongoose');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());

app.use('/', indexRouter);
if (process.env.NODE_ENV === 'local' || process.env.NODE_ENV === 'staging') {
  app.use('/api', apiRouter);
  app.use('/my', angularRouter);
} else if (process.env.NODE_ENV === 'production') {
  app.use(subdomain('api', apiRouter));
  app.use(subdomain('my', angularRouter));
}


// Create link to Angular build directory
// if (process.env.NODE_ENV === 'local') {
// app.use(express.static(path.join(__dirname, '../dist/letsdraw')));
// } else {
app.use(express.static(path.join(__dirname, '../dist')));
// }
app.use(express.static(path.join(__dirname, '../public')));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;