var express = require('express');
var router = express.Router();
const approot = require('app-root-path');

/* Launch Angular APP. */
router.get('/', function (req, res, next) {
  res.sendFile(approot + '/dist/index.html');
});




module.exports = router;