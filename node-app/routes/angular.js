var express = require('express');
var router = express.Router();
var approot = require('app-root-path');

router.get('/', (req, res, next) => {
  console.log("Index.js");
  res.sendFile(approot + '/dist/letsdraw/index.html');
});

module.exports = router;